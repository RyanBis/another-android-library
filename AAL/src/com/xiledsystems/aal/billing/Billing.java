package com.xiledsystems.aal.billing;


public class Billing {
	
	private BillingInterface billIntf;
	
	
	public Billing(BillingInterface billIntf) {
		this.billIntf = billIntf;	
	}
	
	public void setupBilling() {
		billIntf.setupBilling();
	}
	
	public void purchaseItem(String sku, String type) {
		billIntf.purchaseItem(sku, type);
	}
	
	public void setBillingListener(OnBillingEvent listener) {
		billIntf.setBillingListener(listener);
	}
		
}
