package com.xiledsystems.aal.gaming;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Particle extends BaseGameObject {
	
	public static final int STATE_ALIVE = 0;
	public static final int STATE_DEAD = 1;
	public static final int SHAPE_RECT = 0;
	public static final int SHAPE_CIRCLE = 1;
	
	
	public static final int DEFAULT_LIFETIME = 200;
	
	private int maxDimension = 5;
	private int maxSpeed = 10;
	private int shape = SHAPE_RECT;
	private int state;
	private float width;
	private float height;
	private float x, y;
	private double vX, vY;
	private int age;
	private int lifetime;
	private int color;
	private Paint paint;
	
	public Particle(int x, int y) {
		isParticle(true);
		this.x = x;
		this.y = y;
		state = STATE_ALIVE;
		width = rndInt(1, maxDimension);
		height = width;
		lifetime = DEFAULT_LIFETIME;
		age = 0;
		vX = (rndDbl(0, maxSpeed * 2) - maxSpeed);
		vY = (rndDbl(0, maxSpeed * 2) - maxSpeed);
		if (vX * vX + vY * vY > maxSpeed * maxSpeed) {
			vX *= 0.7;
			vY *= 0.7;
		}
		color = Color.argb(255, rndInt(0, 255), rndInt(0, 255), rndInt(0, 255));
		paint = new Paint(color);
	}
	
	public int getMaxSpeed() {
		return maxSpeed;
	}
	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public int getMaxDimension() {
		return maxDimension;
	}
	public void setMaxDimension(int maxDimension) {
		this.maxDimension = maxDimension;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public float getWidth() {
		return width;
	}
	public void setWidth(float width) {
		this.width = width;
	}
	public float getHeight() {
		return height;
	}
	public void setHeight(float height) {
		this.height = height;
	}
	public float getX() {
		return x;
	}
	public void setX(float x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		this.y = y;
	}
	public double getvX() {
		return vX;
	}
	public void setvX(double vX) {
		this.vX = vX;
	}
	public double getvY() {
		return vY;
	}
	public void setvY(double vY) {
		this.vY = vY;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getLifetime() {
		return lifetime;
	}
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public Paint getPaint() {
		return paint;
	}
	public void setPaint(Paint paint) {
		this.paint = paint;
	}
	
	public boolean isAlive() {
		return state == STATE_ALIVE;
	}
	
	
	
	public void reset(int x, int y) {
		state = STATE_ALIVE;
		this.x = x;
		this.y = y;
		age = 0;
	}
	
	private static int rndInt(int from, int to) {
		return (int) (from + Math.random() * (to - from) + 1);
	}
	
	private static double rndDbl(double from, double to) {
		return from + (to - from) * Math.random();
	}
	
	@Override
	public void update(long gameTime) {
		
	}
	
	@Override
	public void draw(Canvas canvas) {
		paint.setColor(color);
		switch (shape) {
			case SHAPE_RECT:
				canvas.drawRect(x, y, x + width, y + height, paint);
				break;
			case SHAPE_CIRCLE:
				canvas.drawCircle(x, y, width, paint);
				break;
		}
	}
	
	private void update() {
		if (state == STATE_ALIVE) {
			x += vX;
			y += vY;			
			int a = color >>> 24;
			a -= 2;
			if (a <= 0) {
				state = STATE_DEAD;
			} else {
				color = (color & 0x00ffffff) + (a << 24);
				paint.setAlpha(a);
				age++;
			}
			if (age > lifetime) {
				state = STATE_DEAD;
			}
		}
	}
	
	@Override
	public void updateParticles(Rect container) {
		if (isAlive()) {
			if (x <= container.left || x >= container.right - width) {
				vX *= -1;
			}			
			if (y <= container.top || y >= container.bottom - height) {
				vY *= -1;
			}
		}
		update();
	}

	@Override
	public void release() {		
	}
}
