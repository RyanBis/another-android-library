package com.xiledsystems.aal.gaming;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class GamePanel extends SurfaceView implements SurfaceHolder.Callback {
	
	private MainGameThread mainThread;
	private OnTouchDown downTouch;
	private OnTouchUp upTouch;
	private OnMove move;
	private int backColor;
	private Bitmap backgroundImage;
	private String avgFPS;
	private boolean showFPS;
	private List<BaseGameObject> objects;
	

	public GamePanel(Context context) {
		this(context, null, 0);
	}

	public GamePanel(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public GamePanel(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		getHolder().addCallback(this);
		mainThread = new MainGameThread(getHolder(), this);
		setFocusable(true);
		objects = new ArrayList<BaseGameObject>();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		mainThread.setRunning(true);
		mainThread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {				
		boolean retry = true;
		while (retry) {
			try {
				mainThread.join();
				retry = false;				
			} catch (InterruptedException e) {				
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (downTouch != null && event.getAction() == MotionEvent.ACTION_DOWN) {
			downTouch.onTouchDown(event.getX(), event.getY());
		}
		if (upTouch != null && event.getAction() == MotionEvent.ACTION_UP) {
			upTouch.onTouchUp(event.getX(), event.getY());
		}
		if (move != null && event.getAction() == MotionEvent.ACTION_MOVE) {
			move.onMove(event.getX(), event.getY());
		}
		return super.onTouchEvent(event);
	}

	@Override
	protected void onDraw(Canvas canvas) {		
	}
	
	public void setOnTouchDownListener(OnTouchDown down) {
		downTouch = down;
	}
	
	public void setOnTouchUpListener(OnTouchUp up) {
		upTouch = up;
	}
	
	public void setOnMoveListener(OnMove move) {
		this.move = move;
	}

	
	public interface OnTouchDown {
		public void onTouchDown(float x, float y);
	}
	
	public interface OnTouchUp {
		public void onTouchUp(float x, float y);
	}
	
	public interface OnMove {
		public void onMove(float x, float y);
	}
	
	public void setAverageFPS(String fps) {
		avgFPS = fps;
	}
	
	public void showFPS(boolean show) {
		showFPS = show;
	}
	
	public void displayFPS(Canvas canvas, String fps) {
		if (canvas != null && fps != null) {
			Paint p = new Paint();
			p.setARGB(255, 255, 255, 255);
			canvas.drawText(fps, this.getWidth() - 50, 20, p);
		}
	}
	
	public void update() {
		// Update all objects
		int size = objects.size();
		long now = System.currentTimeMillis();
		for (int i = 0; i < size; i++) {
			if (objects.get(i).isParticle()) {
				objects.get(i).updateParticles(getHolder().getSurfaceFrame());
			} else {
				objects.get(i).update(now);
			}
		}		
	}
	
	public void render(Canvas canvas) {
		// Render all objects on screen
		if (backColor != -1) {
			canvas.drawColor(backColor);
		} else {
			canvas.drawBitmap(backgroundImage, 0, 0, null);
		}		
		int size = objects.size();
		for (int i = 0; i < size; i++) {
			objects.get(i).draw(canvas);
		}		
		if (showFPS) {
			displayFPS(canvas, avgFPS);
		}
	}
	
	/**
	 * Run this to release all memory objects held by the canvas, and it's children. This should be
	 * called when the panel is no longer needed (usually in onDestroy).
	 */
	public void release() {
		for (BaseGameObject o : objects) {
			o.release();
		}
	}
	
}