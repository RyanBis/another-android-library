package com.xiledsystems.aal.gaming;

import android.graphics.Canvas;
import android.graphics.Rect;

public abstract class BaseGameObject {
	
	private boolean particle = false;
	
	public void isParticle(boolean particle) {
		this.particle = particle;
	}	
	
	public boolean isParticle() {
		return particle;
	}

	public abstract void update(long gameTime);
	
	public abstract void updateParticles(Rect container);
	
	public abstract void draw(Canvas canvas);
	
	public abstract void release();
	
}
