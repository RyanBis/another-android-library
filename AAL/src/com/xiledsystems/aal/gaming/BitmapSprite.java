package com.xiledsystems.aal.gaming;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;


public class BitmapSprite extends BaseSprite {
	
	private Bitmap bitmap;
	private Rect srcRect;
	private Rect destRec;
	
	
	public BitmapSprite(Bitmap bitmap) {
		this.bitmap = bitmap;
		srcRect.right = bitmap.getWidth();
		srcRect.bottom = bitmap.getHeight();
	}
	
	public Bitmap getBitmap() {
		return bitmap;
	}
	
	public void setBitmap(Bitmap bitmap) {
		try {
			this.bitmap.recycle();
		} catch (Exception e) {			
		}
		this.bitmap = bitmap;
	}

	@Override
	public void update(long gameTime) {
	}
	
	public Rect getSrcRect() {
		return srcRect;
	}

	@Override
	public void draw(Canvas canvas) {
		destRec.left = getX();
		destRec.right = getX() + getWidth();
		destRec.top = getY();
		destRec.bottom = getY() + getHeight();
		canvas.drawBitmap(bitmap, srcRect, destRec, null);
	}

	@Override
	public void updateParticles(Rect container) {		
	}
	
	@Override
	public void release() {
		if (bitmap != null) {
			try {
				bitmap.recycle();				
			} catch (Exception e) {				
			}
			bitmap = null;
		}
	}

}
