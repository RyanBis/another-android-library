package com.xiledsystems.aal.util;

import java.util.HashMap;
import java.util.Map;

public abstract class DBObject {
	
	protected Map<String, String> dataToUpdate = new HashMap<String, String>();
	
	public abstract String[] getCreateArray();
	
	public Map<String, String> getUpdatedData() {
		return dataToUpdate;
	}

}
