package com.xiledsystems.aal.util;

import android.content.Context;
import android.media.MediaScannerConnection;


public class MediaUtil {
	
	private MediaUtil() {		
	}
	
	public static void addImageToGallery(Context context, String fullPath) {
		ImageConnectionClient client = new ImageConnectionClient(fullPath);
		MediaScannerConnection connection = new MediaScannerConnection(context, client);
		client.setScanner(connection);
		connection.connect();
	}

}