package com.xiledsystems.aal.util;

import java.security.SecureRandom;
import java.util.Random;

public class Rand {
	
	private static Random random;
	
	private static void checkRandom() {
		if (random == null) {
			random = new SecureRandom();
		}
	}
	
	public static int rndInt(int from, int to) {
		checkRandom();
		int c = random.nextInt(20) + 1;
		int i = 0;
		int r = random.nextInt(to - from) + from;
		while (i < c) {
			i++;
			r = random.nextInt(to - from) + from;
		}
		return r;
	}

}
