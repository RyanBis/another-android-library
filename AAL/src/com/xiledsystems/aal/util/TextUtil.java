package com.xiledsystems.aal.util;

public final class TextUtil {

	private TextUtil() {		
	}
	
	public static boolean isEmpty(String string) {
		return string == null || string.length() < 1;
	}
}
