package com.xiledsystems.aal.util;

import android.content.Context;
import android.content.SharedPreferences;


public class Prefs {
	

	private final Context context;
	private String prefsFileName = "MyPrefs";
	private SharedPreferences prefs;
	

	public Prefs(Context context) {
		this.context = context;
		loadPrefsInstance();
	}

	/**
	 * Sets the filename of this prefs instance.
	 * 
	 * @param filename
	 */
	public void setFileName(String filename) {
		prefsFileName = filename;
		loadPrefsInstance();
	}

	/**
	 * 
	 * @return the filename this prefs instance is storing to
	 */
	public String getFileName() {
		return prefsFileName;
	}

	/**
	 * Store a boolean to shared prefs
	 * 
	 * @param tag The name to store this boolean under
	 * @param valueToStore
	 */
	public void StoreBoolean(String tag, boolean valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putBoolean(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store an integer to prefs
	 * 
	 * @param tag The name to store this integer under
	 * @param valueToStore
	 */
	public void StoreInt(String tag, int valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putInt(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store a Long to prefs
	 * 
	 * @param tag  The name to store this long under
	 * @param valueToStore
	 */
	public void StoreLong(String tag, long valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putLong(tag, valueToStore);
		edit.commit();
	}

	/**
	 * Store a String to prefs
	 * 
	 * @param tag  The name to store this String under
	 * @param valueToStore
	 */
	public void StoreString(String tag, String valueToStore) {
		final SharedPreferences.Editor edit = prefs.edit();
		edit.putString(tag, valueToStore);
		edit.commit();
	}

	/**
	 * 
	 * @param tag The name of the boolean to retrieve from prefs
	 * @return false if none found
	 */
	public boolean GetBoolean(String tag) {
		return prefs.getBoolean(tag, false);
	}

	/**
	 * 
	 * @param tag The name of the integer to retrieve from prefs
	 * @return 0 if none found
	 */
	public int GetInt(String tag) {
		return prefs.getInt(tag, 0);
	}

	/**
	 * 
	 * @param tag The name of the long to retrieve from prefs
	 * @return 0 if none found
	 */
	public long GetLong(String tag) {
		return prefs.getLong(tag, 0);
	}

	/**
	 * 
	 * @param tag The name of the String to retrieve from prefs
	 * @return "" if none found
	 */
	public String GetString(String tag) {
		return prefs.getString(tag, "");
	}

	/**
	 * 
	 * @param tag The name of the boolean to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public boolean GetBoolean(String tag, boolean defValue) {
		return prefs.getBoolean(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the integer to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public int GetInt(String tag, int defValue) {
		return prefs.getInt(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the long to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public long GetLong(String tag, long defValue) {
		return prefs.getLong(tag, defValue);
	}

	/**
	 * 
	 * @param tag The name of the String to retrieve from prefs
	 * @param defValue A default value to return if none found
	 * @return
	 */
	public String GetString(String tag, String defValue) {
		return prefs.getString(tag, defValue);
	}
	
	private void loadPrefsInstance() {
		prefs = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
	}

}