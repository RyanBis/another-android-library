package com.xiledsystems.aal.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;

import android.content.Context;
import android.os.Environment;


public class FileUtil {
	
	private FileUtil() {		
	}
	
	/**
	 * Copies a file from one location to another. The new path is destructive. If you 
	 * copy a file to an existing file's path, that file with be overwritten with the
	 * new content.
	 * Paths expect an absolute file path.
	 * @param srcPath
	 * @param newPath
	 * @return
	 */
	public static boolean copyFile(String srcPath, String newPath) {
		File old = new File(srcPath);
		File newf = new File(newPath);
		if (newf.exists()) {
			newf.delete();
		}
		InputStream is;
		try {			
			is = new FileInputStream(old);			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		BufferedInputStream bin = new BufferedInputStream(is);
		OutputStream out;
		try {
			out = new FileOutputStream(newf);
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
			try {
				bin.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
			return false;
		}
		BufferedOutputStream bout = new BufferedOutputStream(out);
		byte[] buf = new byte[1024];
		try {
			while (bin.read(buf) >= 0) {
				bout.write(buf);
			}
			bin.close();
			if (bout != null) {
				bout.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Moves a file from one location to another. This will delete any existing file
	 * at the new path's location, and delete the original file.
	 * Paths expect an absolute file path.
	 * @param oldPath
	 * @param newPath
	 * @return
	 */
	public static boolean moveFile(String oldPath, String newPath) {
		File old = new File(oldPath);
		File newf = new File(newPath);
		if (newf.exists()) {
			newf.delete();
		}
		InputStream is;
		try {			
			is = new FileInputStream(old);			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		BufferedInputStream bin = new BufferedInputStream(is);
		OutputStream out;
		try {
			out = new FileOutputStream(newf);
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
			try {
				bin.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}			
			return false;
		}
		BufferedOutputStream bout = new BufferedOutputStream(out);
		byte[] buf = new byte[1024];
		try {
			while (bin.read(buf) >= 0) {
				bout.write(buf);
			}
			bin.close();
			if (bout != null) {
				bout.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		old.delete();
		return true;
	}
	
	/**
	 * Store a Serializable object to the app's internal storage. (Files directory)
	 * 
	 * @param context - The app, activity, or service context
	 * @param filename - The name of the file to create for this object
	 * @param object - The object to store
	 * @return true if the object was stored successfully
	 */
	public static boolean storeObject(Context context, String filename, Object object) {
		try {
			FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(object);
			os.flush();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} 
		return true;		
	}
	
	/**
	 * Store a Serializable object to the app's cache directory.
	 * 
	 * @param context The app, activity, or service context
	 * @param filename - The name of the file to create for this object
	 * @param object - The object to store
	 * @return true if the object was stored successfully
	 */
	public static boolean storeObjectToCache(Context context, String filename, Object object) {
		File file = new File(context.getCacheDir(), filename);
		if (file.exists()) {
			file.delete();
		}
		OutputStream out;
		try {
			out = new FileOutputStream(file);
			ObjectOutputStream os = new ObjectOutputStream(out);
			os.writeObject(object);
			os.flush();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Store a Serializable object to the device's external storage
	 * 
	 * @param context The app, activity, or service context
	 * @param filename - The name of the file to create for this object
	 * @param object - The object to store
	 * @return true if the object was stored successfully
	 */
	public static boolean storeObjectToExternal(Context context, String filename, Object object) {
		File file = new File(Environment.getExternalStorageDirectory(), filename);
		if (file.exists()) {
			file.delete();
		}
		OutputStream out;
		try {
			out = new FileOutputStream(file);
			ObjectOutputStream os = new ObjectOutputStream(out);
			os.writeObject(object);
			os.flush();
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * Returns a previously stored Object from the app's internal storage.
	 * 
	 * @param context The app, activity, or service context
	 * @param filename - The name of the file previously created
	 * @return The object that was stored to the specified filename. If none found, null is returned.
	 */
	public static Object getObject(Context context, String filename) {
		Object value = null;
		try {
			FileInputStream filestream = context.openFileInput(filename);
			ObjectInputStream ois = new ObjectInputStream(filestream);
			value = ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
		} catch (StreamCorruptedException e) {
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
		return value;
	}
	
	/**
	 * Returns a previously stored Object from the app's internal storage.
	 * 
	 * @param context The app, activity, or service context
	 * @param filename  The name of the file previously created
	 * @param defaultValue If an error, or no file found, the default value will be returned
	 * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
	 */
	public static Object getObject(Context context, String filename, Object defaultValue) {
		Object value = defaultValue;
		try {
			FileInputStream filestream = context.openFileInput(filename);
			ObjectInputStream ois = new ObjectInputStream(filestream);
			value = ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
		} catch (StreamCorruptedException e) {
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
		return value;
	}
	
	/**
	 * Returns a previously stored Object from the app's cache directory.
	 * 
	 * @param context The app, activity, or service context
	 * @param filename - The name of the file previously created
	 * @return The object that was stored to the specified filename. If none found, null is returned.
	 */
	public static Object getObjectFromCache(Context context, String filename) {
		Object value = null;
		try {
			File file = new File(context.getCacheDir(), filename);
			if (file.exists()) {			
				FileInputStream filestream = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(filestream);
				value = ois.readObject();
				ois.close();
			}
		} catch (IOException e) {
			e.printStackTrace();			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();			
		}
		return value;
	}
	
	/**
	 * Returns a previously stored Object from the app's cache directory.
	 * 
	 * @param context The app, activity, or service context
	 * @param filename  The name of the file previously created
	 * @param defaultValue If an error, or no file found, the default value will be returned
	 * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
	 */
	public static Object getObjectFromCache(Context context, String filename, Object defaultValue) {
		Object value = new Object();
		try {
			File file = new File(context.getCacheDir(), filename);
			if (!file.exists()) {				
				return defaultValue;
			}
			FileInputStream filestream = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(filestream);
			value = ois.readObject();
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
			return defaultValue;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return defaultValue;
		}
		return value;
	}
	
	/**
	 * Returns a previously stored Object from the device's external storage.
	 *
	 * @param filename - The name of the file previously created
	 * @return The object that was stored to the specified filename. If none found, null is returned.
	 */
	public static Object getObjectFromExternal(String filename) {
		Object value = null;
		try {
			File file = new File(Environment.getExternalStorageDirectory(), filename);
			if (file.exists()) {			
				FileInputStream filestream = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(filestream);
				value = ois.readObject();
				ois.close();
			}
		} catch (IOException e) {
			e.printStackTrace();			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();			
		}
		return value;
	}
	
	/**
	 * Returns a previously stored Object from the device's external storage.
	 *	
	 * @param filename  The name of the file previously created
	 * @param defaultValue If an error, or no file found, the default value will be returned
	 * @return The object that was stored to the specified filename. If none found, the defaultValue is returned.
	 */
	public static Object getObjectFromExternal(String filename, Object defaultValue) {
		Object value = new Object();
		try {
			File file = new File(Environment.getExternalStorageDirectory(), filename);
			if (!file.exists()) {				
				return defaultValue;
			}
			FileInputStream filestream = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(filestream);
			value = ois.readObject();
			ois.close();
		} catch (IOException e) {
			e.printStackTrace();
			return defaultValue;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return defaultValue;
		}
		return value;
	}

}