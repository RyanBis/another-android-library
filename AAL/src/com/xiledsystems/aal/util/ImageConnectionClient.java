package com.xiledsystems.aal.util;

import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;

public class ImageConnectionClient implements MediaScannerConnectionClient {

	String filename;
	MediaScannerConnection mediaScannerConnection;
	
	public ImageConnectionClient(String name) {
		filename = name;
	}
	
	public void setScanner(MediaScannerConnection msc) {
		mediaScannerConnection = msc;
	}

	@Override
	public void onMediaScannerConnected() {
		mediaScannerConnection.scanFile(filename, null);
		
	}

	@Override
	public void onScanCompleted(String arg0, Uri arg1) {
		if (arg0.equals(filename)) {
			mediaScannerConnection.disconnect();
		}		
	}

}