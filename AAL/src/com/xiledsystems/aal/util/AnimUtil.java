package com.xiledsystems.aal.util;

import android.content.Context;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;


public class AnimUtil {

	private final Context context;
	private AnimationListener animListener = null;

	public AnimUtil(Context context) {
		this.context = context;
	}

	public void setListener(AnimationListener listener) {
		animListener = listener;
	}

	public void Animate(View view, int resId) {
		Animate(context, view, resId, animListener);
	}

	public static void FadeIn(View view, int duration, AnimationListener listener) {
		AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		view.startAnimation(anim);
	}
	
	public static void FadeOut(View view, int duration, AnimationListener listener) {
		AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
		anim.setDuration(duration);
		anim.setAnimationListener(listener);
		view.startAnimation(anim);
	}

	public static void Animate(Context context, View view, int animationRes, AnimationListener listener) {
		Animation anim = AnimationUtils.loadAnimation(context, animationRes);
		if (listener != null) {
			anim.setAnimationListener(listener);
		}
		view.startAnimation(anim);
	}


}