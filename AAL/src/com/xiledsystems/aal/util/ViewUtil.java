package com.xiledsystems.aal.util;

import android.view.View;

public class ViewUtil {
	
	private ViewUtil() {		
	}
	
	/**
	 * This will hide the view from being drawn on screen.
	 * 
	 * @param view
	 */
	public static void hideView(View view) {
		view.setVisibility(View.GONE);
	}
	
	/**
	 * Show a view on screen, whether it is hidden, or invisible
	 * 
	 * @param view
	 */
	public static void unHideView(View view) {
		view.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Make a view invisible. The view will still take up space on
	 * screen.
	 * 
	 * @param view
	 */
	public static void makeInvisible(View view) {
		view.setVisibility(View.INVISIBLE);
	}

}
