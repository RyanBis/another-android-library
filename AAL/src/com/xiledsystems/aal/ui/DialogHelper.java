package com.xiledsystems.aal.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

public class DialogHelper {

	private final Context context;
	private static Dialog curDialog;
	private static boolean useCustom = false;
	private static int layoutId = -1;
	private static boolean fullSize;

	public DialogHelper(Context context) {
		this.context = context;
	}

	public void setLayoutResourceId(int resourceId) {
		layoutId = resourceId;
	}

	public int getLayoutResourceId() {
		return layoutId;
	}

	public void showBasicDialog(final String title, final String message) {
		showBasicDialog(context, title, message);
	}

	public void showDialog(final String title, final String message, final DialogListener clickListener, final String... buttonText) {
		showDialog(context, title, message, clickListener, buttonText);
	}

	public boolean isShowing() {
		return curDialog != null && curDialog.isShowing();
	}

	public static void useCustomLayout(boolean custom) {
		useCustom = custom;
	}

	public static boolean isCustomLayout() {
		return useCustom;
	}
	
	public static void showToast(Context context, String message) {
		showtoast(context, message, Toast.LENGTH_LONG);
	}
	
	private static void showtoast(final Context context, final String message, final int length) {
		if (!onMainThread()) {
			getHandler().post(new Runnable() {				
				@Override
				public void run() {
					Toast.makeText(context, message, length).show();
				}
			});
		} else {
			Toast.makeText(context, message, length).show();
		}
	}
	
	public static void showToastShort(final Context context, final String message) {
		showtoast(context, message, Toast.LENGTH_SHORT);
	}

	/**
	 * Displays a basic dialog message. This adds an "OK" button on the button which just dismisses the dialog. This is
	 * safe to call from other threads.
	 * 
	 * @param context
	 * @param title
	 * @param message
	 */
	public static void showBasicDialog(final Context context, final String title, final String message) {
		if (!onMainThread()) {
			getHandler().post(new Runnable() {
				@Override
				public void run() {
					if (useCustom) {
						curDialog = buildBasicCustomDialog(context, layoutId, title, message).setPositiveButton("OK", null).create();
						show();
					} else {
						curDialog = buildBasicDialog(context, title, message).setPositiveButton("OK", null).create();
						show();
					}
				}
			});
		} else {
			if (useCustom) {
				curDialog = buildBasicCustomDialog(context, layoutId, title, message).setPositiveButton("OK", null).create();
				show();
			} else {
				curDialog = buildBasicDialog(context, title, message).setPositiveButton("OK", null).create();
				show();
			}
		}
	}
	
	public static void setFull(boolean full) {
		fullSize = full;
	}
	
	private static void show() {
		curDialog.show();
		if (fullSize) {
			curDialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		}
	}

	/**
	 * Show a dialog to the user. This is safe to call from any thread (it will get posted to the UI thread if it is
	 * called from anywhere else). If the button text array is over 3 in size, an exception will be thrown. An exception
	 * will also be thrown if the listener is null.
	 * 
	 * @param context
	 * @param title
	 * @param message
	 * @param clickListener
	 * @param buttonText
	 */
	public static void showDialog(final Context context, final String title, final String message, final DialogListener clickListener,
			final String... buttonText) {
		if (buttonText == null || buttonText.length == 0 && clickListener == null) {
			showBasicDialog(context, title, message);
		} else {
			if (useCustom) {
				final CustomDialog.Builder builder = buildBasicCustomDialog(context, layoutId, title, message);
				addCustomButtons(builder, buttonText, clickListener);
				curDialog = builder.create();
			} else {
				final AlertDialog.Builder builder = buildBasicDialog(context, title, message);
				addDefaultButtons(builder, buttonText, clickListener);
				curDialog = builder.create();
			}
			if (onMainThread()) {
				show();
			} else {
				getHandler().post(new Runnable() {
					@Override
					public void run() {
						show();
					}
				});
			}
		}
	}
	
	private static void addCustomButtons(CustomDialog.Builder builder, final String[] buttonText, final DialogListener listener) {
		int btnCount = buttonText.length;
		if (btnCount > 3) {
			throw new IllegalStateException("The dialog only allows 3 buttons, " + btnCount + " titles for buttons were input!");
		}
		if (listener == null) {
			throw new IllegalStateException("Button listener cannot be null.");
		}
		builder.setPositiveButton(buttonText[0], new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.buttonClicked(buttonText[0]);
			}
		});
		if (btnCount > 1) {
			builder.setNegativeButton(buttonText[1], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[1]);
				}
			});
		}
		if (btnCount > 2) {
			builder.setNegativeButton(buttonText[2], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[2]);
				}
			});
		}		
	}

	private static void addDefaultButtons(AlertDialog.Builder builder, final String[] buttonText, final DialogListener listener) {
		int btnCount = buttonText.length;
		if (btnCount > 3) {
			throw new IllegalStateException("The dialog only allows 3 buttons, " + btnCount + " titles for buttons were input!");
		}
		if (listener == null) {
			throw new IllegalStateException("Button listener cannot be null.");
		}
		builder.setPositiveButton(buttonText[0], new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				listener.buttonClicked(buttonText[0]);
			}
		});
		if (btnCount > 1) {
			builder.setNegativeButton(buttonText[1], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[1]);
				}
			});
		}
		if (btnCount > 2) {
			builder.setNegativeButton(buttonText[2], new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					listener.buttonClicked(buttonText[2]);
				}
			});
		}		
	}

	private static AlertDialog.Builder buildBasicDialog(Context context, String title, String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle(title);
		builder.setMessage(message);
		return builder;
	}

	private static CustomDialog.Builder buildBasicCustomDialog(Context context, int layoutId, String title, String message) {
		CustomDialog.Builder builder = new CustomDialog.Builder(context, layoutId);
		builder.setTitle(title);
		builder.setMessage(message);
		return builder;
	}

	private static Handler getHandler() {
		return new Handler(Looper.getMainLooper());
	}

	private static boolean onMainThread() {
		return Thread.currentThread() == Looper.getMainLooper().getThread();
	}

	public interface DialogListener {
		public void buttonClicked(String buttonText);
	}

}