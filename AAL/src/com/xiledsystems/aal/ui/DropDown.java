package com.xiledsystems.aal.ui;

import java.util.ArrayList;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


public class DropDown {
	
	private final Activity context;
	private Spinner spinner;
	private String[] items;
	private ArrayAdapter<CharSequence> spinnerAdapter;
	private int buttonLayout = android.R.layout.simple_spinner_item;
	private int textId = android.R.id.text1;
	private int dropDownLayout = android.R.layout.simple_spinner_dropdown_item;
	
	
	/**
	 * Constructor for DropDown view.
	 * 
	 * @param context 
	 * @param resId The resource Id of the spinner placed in xml.
	 */
	public DropDown(Activity context, int resId) {
		this.context = context;
		spinner = (Spinner) context.findViewById(resId);
		if (spinner == null) {
			throw new RuntimeException("DropDown view resource is null!");
		}
	}
	
	/**
	 * Constructor for DropDown view when inflating a layout in xml.
	 * You'd use this if you're inflating another layout which contains
	 * the spinner.
	 * 
	 * @param context
	 * @param inflatedLayout The inflated view which contains the spinner
	 * @param resId The resource id of the spinner contained in the inflated xml layout
	 */
	public DropDown(Activity context, View inflatedLayout, int resId) {
		this.context = context;
		spinner = (Spinner) inflatedLayout.findViewById(resId);
		if (spinner == null) {
			throw new RuntimeException("DropDown view resource is null!");
		}
	}
	
	/**
	 * Sets up the items, and all customization resources
	 * 
	 * @param items
	 * @param buttonLayout The layout id of the layout of the button of the dropdown
	 * @param dropDownLayout The layout id of a dropdown item
	 * @param textViewId The textview resource id contained in both layouts (they must both use the same id)
	 */
	public void setup(String[] items, int buttonLayout, int dropDownLayout, int textViewId) {
		this.buttonLayout = buttonLayout;
		this.dropDownLayout = dropDownLayout;
		textId = textViewId;
		this.items = items;
		resetAdatper();
	}
	
	/**
	 * Sets up the items, and all customization resources
	 * 
	 * @param items
	 * @param buttonLayout The layout id of the layout of the button of the dropdown
	 * @param dropDownLayout The layout id of a dropdown item
	 * @param textViewId The textview resource id contained in both layouts (they must both use the same id)
	 */
	public void setup(ArrayList<String> items, int buttonLayout, int dropDownLayout, int textViewId) {
		this.buttonLayout = buttonLayout;
		this.dropDownLayout = dropDownLayout;
		textId = textViewId;
		this.items = items.toArray(new String[0]);
		resetAdatper();
	}
	
	/**
	 * Sets the custom resource ids for the dropdown view.
	 * 
	 * @param buttonLayout the layout id of the layout of the button
	 * @param dropDownLayout the layout id of a dropdown item
	 * @param textViewId the textview resource id contained in both layouts
	 */
	public void setLayouts(int buttonLayout, int dropDownLayout, int textViewId) {
		this.buttonLayout = buttonLayout;
		this.dropDownLayout = dropDownLayout;
		textId = textViewId;
		if (items != null) {
			resetAdatper();
		}
	}
	
	public void setButtonLayout(int layoutResId) {
		buttonLayout = layoutResId;
	}
	
	private void resetAdatper() {
		spinnerAdapter = new ArrayAdapter<CharSequence>(context, buttonLayout, textId, items);
		spinnerAdapter.setDropDownViewResource(dropDownLayout);
		spinner.setAdapter(spinnerAdapter);
	}
	
	public void setonItemClickListener(OnItemClickListener listener) {
		spinner.setOnItemClickListener(listener);		
	}
	
	public void setItems(ArrayList<String> items) {
		this.items = items.toArray(new String[0]);
		resetAdatper();
	}
	
	public void setItems(String[] items) {
		this.items = items;
		resetAdatper();
	}
	
	/**
	 * 
	 * @return the currently selected item
	 */
	public String getSelectedItem() {
		return spinner.getSelectedItem().toString();
	}
	
	public int getSelectedPosition() {
		return spinner.getSelectedItemPosition();
	}
	
	public void setSelectedPosition(int position) {
		spinner.setSelection(position);		
	}
	
	/**
	 * 
	 * @return the android Spinner backing the DropDown
	 */
	public Spinner getSpinner() {
		return spinner;
	}

}
