package com.xiledsystems.aal.sql;


import android.content.ContentValues;


public class Insert {
	
	private String table;	
	private ContentValues values = new ContentValues();
	
	public Insert into(String table) {
		this.table = table;
		return this;
	}
	
	public Insert inColumn(String columnName, String data) {
		values.put(SqlDb.checkColumnName(columnName), data);
		return this;		
	}
	
	public long execute(SqlDb db) {
		return db.insert(table, values);
	}

}
