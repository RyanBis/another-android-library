package com.xiledsystems.aal.sql;

import android.content.ContentValues;

public class Update {
	
	private String where;
	private String[] whereArgs;
	private String table;
	private ContentValues values;	
	
	public Update update(String table) {
		this.table = table;		
		return this;
	}
	
	public Update with(ContentValues values) {
		this.values = values;
		return this;
	}
	
	public Update where(String where) {
		this.where = where;
		return this;
	}
	
	public Update whereArgs(String[] whereArgs) {
		this.whereArgs = whereArgs;
		return this;
	}
	
	public int execute(SqlDb db) {		
		return db.update(table, values, where, whereArgs);		
	}

}