package com.xiledsystems.aal.sql;

import java.util.ArrayList;
import java.util.Arrays;


public class Query extends BaseQuery {
	
	private StringBuilder statement = new StringBuilder();
	
	private ArrayList<String> values;
		
	/**
	 * Use select() to return all columns. Otherwise, provide a string array of the columns
	 * you wish returned. (For one, you can just use a single string)
	 * 
	 * This should be the first call made. Only call it once!
	 * 
	 * @param whatToSelect
	 * @return
	 */
	public Query select(String... whatToSelect) {
		statement.append(SELECT);
		if (whatToSelect == null || whatToSelect.length == 0) {
			statement.append(" * ");
		} else {			
			statement.append(" " + getString(whatToSelect));
		}
		return this;
	}
	
	/**
	 * Sets the table to pull this query from. This will throw an IllegalArgumentException if the
	 * table name contains a single quotation mark.	 
	 * 
	 * @param tableName
	 * @return
	 */
	public Query from(String tableName) {
		statement.append(FROM);
		if (tableName.contains("'")) {
			throw new IllegalArgumentException("Table names cannot contain single quotation marks.");
		}
		statement.append(" " + SqlDb.checkTableName(tableName) + " ");
		return this;
	}
		
	/**
	 * Set the where statement. This method will throw an IllegalArgumentException if the column
	 * name contains a double quotation mark.
	 * 
	 * This can be called multiple times, for more complex queries.
	 * 
	 * @param columnName
	 * @return
	 */
	public Query where(String columnName) {
		statement.append(WHERE);
		if (columnName.contains("\"")) {
			throw new IllegalArgumentException("Column names cannot contain double quotation marks.");
		}
		statement.append(" " + SqlDb.checkColumnName(columnName) + " ");
		return this;
	}
			
	public Query equalTo(String value) {
		statement.append("=? ");
		checkValues();
		values.add(value);
		return this;
	}
	
	public Query lessThan(String value) {
		statement.append("<? ");
		checkValues();
		values.add(value);
		return this;
	}
	
	public Query lessThanEqual(String value) {
		statement.append("<=? ");
		checkValues();
		values.add(value);
		return this;
	}
	
	public Query greaterThan(String value) {
		statement.append(">? ");
		checkValues();
		values.add(value);
		return this;
	}
	
	public Query greaterThanEqual(String value) {
		statement.append(">=? ");
		checkValues();
		values.add(value);
		return this;
	}
	
	public Query as(String as) {
		statement.append(AS);
		statement.append(" " + as + " ");
		return this;
	}
	
	public Query join(String table) {
		statement.append(JOIN);
		statement.append(" " + table + " ");
		return this;
	}
	
	public Query on(String onstatement) {
		statement.append(ON);
		statement.append(" " + onstatement + " ");
		return this;
	}
	
	public Query like(String value) {
		statement.append(LIKE);
		statement.append(" '%" + value + "%' ");		
		return this;
	}
	
	public Query and() {
		statement.append(AND + " ");
		return this;
	}
	
	/*
	 * Open parenthesis. Don't foget to call clp to close the parenthesis. This is useful when
	 * making a more robust query (for example, you want one column to match, and one of many
	 * others).
	 */
	public Query opp() {
		statement.append("( ");
		return this;
	}
	
	/**
	 * Close parenthesis.
	 * @return
	 */
	public Query clp() {
		statement.append(") ");
		return this;
	}
	
	public Query or() {
		statement.append(OR + " ");
		return this;
	}
	
	public Query order(String order) {
		statement.append(ORDER);
		statement.append(" " + order + " ");
		return this;
	}
	
	public Query limit(String limit) {
		statement.append(LIMIT);
		statement.append(" " + limit);
		return this;
	}
	
	public Query limit(int limit) {
		statement.append(LIMIT);
		statement.append(" " + limit);
		return this;
	}
	
	public Query manualStatement(String statement) {
		this.statement = new StringBuilder();
		this.statement.append(statement);
		return this;
	}
	
	public Query manualStatement(String statement, String[] args) {
		this.statement = new StringBuilder();
		this.statement.append(statement);
		this.values = new ArrayList<String>(Arrays.asList(args));
		return this;
	}	
	
	private void checkValues() {
		if (values == null) {
			values = new ArrayList<String>();
		}
	}
		
	private static String getString(String[] what) {		
		StringBuilder b = new StringBuilder();
		for (int i = 0; i < what.length; i++) {			
			b.append(SqlDb.checkColumnName(what[i]));
			if (i != what.length - 1) {
				b.append(", ");
			} else {
				b.append(" ");
			}
		}
		return b.toString();
	}

	@Override
	public String getStatement() {
		if (statement != null) {
			return statement.toString();
		}
		return "";
	}

	@Override
	public String[] getSelectionArgs() {
		if (values == null) {
			return null;
		} else {
			return values.toArray(new String[0]);
		}
	}

}