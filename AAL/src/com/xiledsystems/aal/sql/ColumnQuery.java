package com.xiledsystems.aal.sql;

import java.util.ArrayList;

import android.database.Cursor;

public class ColumnQuery extends BaseQuery {
	
	private final String columnName;
	private final String tableName;
	
	public ColumnQuery(String table, String name) {
		columnName = name;
		tableName = table;
	}
	
	public ArrayList<String> executeList(SqlDb db) {
		Object lock = db.getLock();
		ArrayList<String> results = new ArrayList<String>();
		synchronized (lock) {
			final Cursor cursor = db.query(this);
			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						results.add(cursor.getString(0));
					} while (cursor.moveToNext());
				}
				cursor.close();
			}
		}
		return results;		
	}

	@Override
	public String getStatement() {		
		return new Query().select(columnName).from(tableName).getStatement();
	}

	@Override
	public String[] getSelectionArgs() {
		return null;
	}

}