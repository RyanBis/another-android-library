package com.xiledsystems.aal.googlebilling;


import android.app.Activity;
import android.content.Intent;
import com.xiledsystems.aal.billing.BillingInterface;
import com.xiledsystems.aal.billing.OnBillingEvent;
import com.xiledsystems.aal.googlebilling.util.IabHelper;
import com.xiledsystems.aal.googlebilling.util.IabHelper.OnIabSetupFinishedListener;
import com.xiledsystems.aal.googlebilling.util.IabResult;
import com.xiledsystems.aal.googlebilling.util.Inventory;
import com.xiledsystems.aal.googlebilling.util.Purchase;


public class GoogleBilling implements BillingInterface {
	
	private final Activity context;
	private final static String PAY = ".payload";
	private final static String NULL_DATA_ERROR = "IAB returned null purchaseData or dataSignature";
	private final static String TAG = "GoogleBillingComponent";
	public final static String ITEM_TYPE_SUBS = IabHelper.ITEM_TYPE_SUBS;
	public final static String ITEM_TYPE_INAPP = IabHelper.ITEM_TYPE_INAPP;
	public final static int PURCHASED = 0;
	public final static int CANCELLED = 1;
	public final static int REFUNDED = 2;

	private int requestCode;
	private String payload;
	private String base64Key;

	private String tmpSku = null;

	private IabHelper helper;

	private InventoryListener gotInventoryListener = new InventoryListener();
	private PurchaseFinishedListener purchaseFinsihedListener = new PurchaseFinishedListener();
	private OnBillingEvent billingListener;
	
	
	public GoogleBilling(Activity context, String base64Key) {
		this.context = context;
		payload = context.getPackageName() + PAY;
		Base64Key(base64Key);		
	}
	
	
	/**
	 * Set a developer payload, or tag, to add more verification. A default payload of the packagename + .payload is
	 * used if this is not set.
	 * 
	 * @param payload
	 */
	public void Payload(String payload) {
		this.payload = payload;
	}

	/**
	 * 
	 * @return - The current payload.
	 */
	public String Payload() {
		return payload;
	}
	
	/**
	 * 
	 * @param p
	 *            - the purchase to check
	 * @return - Whether this payload is verified (requested with our payload)
	 */
	public boolean verifyPayload(Purchase p) {
		String payLoad = p.getDeveloperPayload();
		return payLoad.contains(payload);
	}

	/**
	 * This needs to be set before calling SetupBilling. This is found in the developer's console.
	 * 
	 * @param key
	 */
	public void Base64Key(String key) {
		base64Key = key;
		helper = new IabHelper(context, key);
	}

	/**
	 * 
	 * @return - The currently used key (if not set, null is returned)
	 */
	public String Base64Key() {
		return base64Key;
	}

	/**
	 * Setting this to true will enable more verbose logging to be output to the logcat. Do not leave this true in a
	 * production application.
	 * 
	 * @param debug
	 */
	public void VerboseDebug(boolean debug) {
		if (helper != null) {
			helper.enableDebugLogging(debug, TAG);
		} else {
			throw new GoogleBillingException("Base64 key not set yet!");
		}
	}

	/**
	 * 
	 * @return the IabHelper this component is backed by.
	 */
	public IabHelper getHelper() {
		return helper;
	}

	/**
	 * This will query the inventory of purchases. This is run automatically after BillingSetup is run. In most cases,
	 * you won't have to use this method.
	 * 
	 */
	public void queryInventory() {
		helper.queryInventoryAsync(gotInventoryListener);
	}

	private void requeryInventory() {
		helper.queryInventoryAsync(new InventoryListener2());
	}

	/**
	 * This gets google's in-app billing started, and setup. It does the basic checks for compatibility, and whatnot. If
	 * there's an error, the BILLING_ERROR event will be thrown, with the explanation in args[0].
	 * 
	 * This will throw the INVENTORY_RESULT event if the setup is successful, which returns the current inventory of
	 * purchases, if any (The result is args[0], which is the IabResult class, and args[1] is the Inventory class).
	 * 
	 * You must set the Base64Key (in Google's developer console) before running this method.
	 */
	public void SetupBilling() {
		if (helper != null) {
			helper.startSetup(new SetupFinishedListener());
		} else {
			throw new GoogleBillingException("Base64 key not set yet!");
		}
	}
	
	private class GoogleBillingException extends RuntimeException {
		/**
		 * 
		 */
		private static final long serialVersionUID = -6899938259178520093L;

		public GoogleBillingException(String message) {
			super(message);
		}
	}
	
	private class InventoryListener implements IabHelper.QueryInventoryFinishedListener {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				billingListener.onBillingEvent(BILLING_ERROR, "Failure querying inventory. Error: " + result.toString());
			} else {				
				billingListener.onBillingEvent(INVENTORY_RETURN, inv);
			}
		}
	}
	
	private class InventoryListener2 implements IabHelper.QueryInventoryFinishedListener {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (result.isFailure()) {
				billingListener.onBillingEvent(BillingInterface.BILLING_ERROR, "Failure querying inventory. Error: " + result.toString());				
			} else {
				billingListener.onBillingEvent(BillingInterface.PURCHASE_SUCCESS, result, inv.getPurchase(tmpSku));				
				tmpSku = null;
			}
		}
	}
	
	private class SetupFinishedListener implements OnIabSetupFinishedListener {
		@Override
		public void onIabSetupFinished(IabResult result) {
			if (!result.isSuccess()) {
				billingListener.onBillingEvent(BillingInterface.BILLING_ERROR, "Problem setting up in-app " + "billing. " + "Error: "
						+ result);				
			} else {
				helper.queryInventoryAsync(gotInventoryListener);
			}
		}
	}
	
	private class PurchaseFinishedListener implements IabHelper.OnIabPurchaseFinishedListener {
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase info) {
			if (result.isFailure()) {
				// Hack to fix the null purchasedata/dataSignature issue
				if (result.toString().contains(NULL_DATA_ERROR) && tmpSku != null) {
					requeryInventory();
				} else {
					billingListener.onBillingEvent(BillingInterface.BILLING_ERROR, "Error purchasing. Error: " + result.toString());					
				}
			} else if (!verifyPayload(info)) {
				billingListener.onBillingEvent(BillingInterface.BILLING_ERROR, "Error purchasing. Payload verification failed.");				
			} else {
				billingListener.onBillingEvent(BillingInterface.PURCHASE_SUCCESS, info.getSku());				
			}
		}
	}
	
	@Override
	public void setupBilling() {
		helper.startSetup(new SetupFinishedListener());		
	}

	@Override
	public void purchaseItem(String sku, String type) {
		tmpSku = sku;
		helper.launchPurchaseFlow(context, sku, requestCode, purchaseFinsihedListener, payload);		
	}

	@Override
	public void setBillingListener(OnBillingEvent listener) {
		billingListener = listener;
	}

	@Override
	public void onResume() {		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		helper.handleActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void close() {
		helper.dispose();
	}	

}