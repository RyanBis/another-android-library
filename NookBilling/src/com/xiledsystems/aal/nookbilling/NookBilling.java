package com.xiledsystems.aal.nookbilling;

import java.util.HashSet;
import java.util.Set;
import mp.MpUtils;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import com.xiledsystems.aal.billing.BillingInterface;
import com.xiledsystems.aal.billing.OnBillingEvent;


public class NookBilling implements BillingInterface {

	private final Activity context;
	private OnBillingEvent billingListener;
	private Set<NookService> services;
	private String manifestString;

	
	/**
	 * You must remember to add the following entries to your AndroidManifest.xml file, in the
	 * application node. (You can replace com.xiledsystems.aal.nookbilling with your app's
	 * package name)
	 * 
	 * <service android:name="mp.MpService" />
     *   <service android:name="mp.StatusUpdateService" />
	 *
     *   <activity
     *       android:name="mp.MpActivity"
     *       android:configChanges="orientation|keyboardHidden|screenSize"
     *       android:theme="@android:style/Theme.Translucent.NoTitleBar" />
	 *
     *   <receiver
     *       android:name="com.xiledsystems.aal.nookbilling.NookBilling$PaymentReceiver"
     *       android:permission="com.xiledsystems.aal.nookbilling.PAYMENT_INFO_BROADCAST_PERMISSION" >
     *       <intent-filter>
     *          <action android:name="com.xiledsystems.aal.nookbilling.PAYMENT_STATUS_CHANGED" />
     *      </intent-filter>
     *   </receiver>
	 * 
	 * The manPermission string is Manifest.permission.PAYMENT_INFO_BROADCAST_PERMISSION) *** Make sure you
	 * import the Manifest from your package NOT android.Manifest.
	 * 
	 * @param context
	 * @param services
	 * @param secret
	 * @param manPermission
	 */
	public NookBilling(Activity context, Set<NookService> services,	String manPermission) {
		this.context = context;
		this.services = services;
		this.manifestString = manPermission;		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setupBilling() {
		new AsyncTask<Set<NookService>, Set<NookService>, Set<NookService>>() {
			@Override
			protected void onPostExecute(Set<NookService> result) {
				billingListener.onBillingEvent(INVENTORY_RETURN, result);
			}

			@Override
			protected Set<NookService> doInBackground(Set<NookService>... skus) {
				Set<NookService> pskus = new HashSet<NookService>();
				for (NookService service : skus[0]) {
					int status = MpUtils.getNonConsumablePaymentStatus(context,
							service.getServiceId(), service.getSecret(),
							service.getFullName());
					if (status == MpUtils.MESSAGE_STATUS_BILLED) {
						pskus.add(service);
					}
				}
				return pskus;
			}
		}.execute(services);
	}

	private static NookService getService(Set<NookService> services,
			String serviceId) {
		for (NookService s : services) {
			if (s.getServiceId().equals(serviceId)) {
				return s;
			}
		}
		return null;
	}

	@Override
	public void purchaseItem(String sku, String type) {
		NookService ns = getService(services, sku);
		if (ns != null) {
			Intent intent = new Intent(context, NookPayActivity.class);
			intent.putExtra(NookPayActivity.PURCHASE_REQUEST, true);
			intent.putExtra(NookPayActivity.SERVICE_ID, ns.getServiceId());
			intent.putExtra(NookPayActivity.SECRET, ns.getSecret());
			intent.putExtra(NookPayActivity.FULL_NAME, ns.getFullName());
			intent.putExtra(NookPayActivity.MAN_PERM_STRING, manifestString);
			context.startActivity(intent);
		}
	}

	@Override
	public void setBillingListener(OnBillingEvent listener) {
		billingListener = listener;
	}

	@Override
	public void onResume() {
	}

	@Override
	public void close() {
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	}
	
	@SuppressWarnings("unused")
	private class PaymentReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			Bundle extras = intent.getExtras();
			int billingStatus = extras.getInt("billing_status");
			switch (billingStatus) {
				case MpUtils.MESSAGE_STATUS_BILLED:
					String service_Id = extras.getString("service_id");
					NookService ns = getService(services, service_Id);					
					billingListener.onBillingEvent(PURCHASE_SUCCESS, service_Id, ns);					
					break;					
			}
		}
	}

}