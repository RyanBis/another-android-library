package com.xiledsystems.aal.nookbilling;

import android.content.Intent;
import android.os.Bundle;
import mp.MpUtils;
import mp.PaymentActivity;
import mp.PaymentRequest.PaymentRequestBuilder;


public class NookPayActivity extends PaymentActivity {
	
	public static final String PURCHASE_REQUEST = "RequestPurchase";
	public static final String SERVICE_ID = "ServiceId";
	public static final String SECRET = "InAppSecret";
	public static final String MAN_PERM_STRING = "ManifestPermissionString";
	public static final String FULL_NAME = "FullProductName";
	
	private String serviceId;
	private String inappSecret;
	private String manString;
	private String fullName;
	private boolean purchase;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		Intent intent = getIntent();
		if (intent.hasExtra(PURCHASE_REQUEST)) {
			purchase = true;
			serviceId = intent.getStringExtra(SERVICE_ID);
			inappSecret = intent.getStringExtra(SECRET);
			manString = intent.getStringExtra(MAN_PERM_STRING);
			fullName = intent.getStringExtra(FULL_NAME);
		}
		MpUtils.enablePaymentBroadcast(this, manString);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		if (purchase) {
			PaymentRequestBuilder builder = new PaymentRequestBuilder();
			builder.setDisplayString(fullName);
			builder.setService(serviceId, inappSecret);
			builder.setProductName(fullName);
			builder.setConsumable(false);
			makePayment(builder.build());
			finish();
		}
	}

}