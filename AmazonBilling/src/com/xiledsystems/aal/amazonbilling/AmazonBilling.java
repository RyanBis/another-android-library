package com.xiledsystems.aal.amazonbilling;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import android.content.Context;
import android.content.Intent;
import com.amazon.inapp.purchasing.BasePurchasingObserver;
import com.amazon.inapp.purchasing.GetUserIdResponse;
import com.amazon.inapp.purchasing.GetUserIdResponse.GetUserIdRequestStatus;
import com.amazon.inapp.purchasing.Item;
import com.amazon.inapp.purchasing.ItemDataResponse;
import com.amazon.inapp.purchasing.Offset;
import com.amazon.inapp.purchasing.PurchaseResponse;
import com.amazon.inapp.purchasing.PurchaseUpdatesResponse;
import com.amazon.inapp.purchasing.PurchasingManager;
import com.amazon.inapp.purchasing.Receipt;
import com.xiledsystems.aal.billing.BillingInterface;
import com.xiledsystems.aal.billing.OnBillingEvent;
import com.xiledsystems.aal.util.Prefs;


public class AmazonBilling implements BillingInterface {
	
	private final static String USERID = "AmazonUserId";
	private final static String OFFSET = "StoredOffset";
	private BillingObserver observer;
	private String requestId;
	private Prefs prefs;
	private OnBillingEvent billingListener;
	private Set<String> skus;
	
	/**
	 * You must remember to add the following to your AndroidManifest.xml, within the
	 * application node.
	 * 
	 * <receiver android:name="com.amazon.inapp.purchasing.ResponseReceiver" >
     *       <intent-filter>
     *           <action
     *               android:name="com.amazon.inapp.purchasing.NOTIFY"
     *               android:permission="com.amazon.inapp.purchasing.Permission.NOTIFY" />
     *       </intent-filter>
     *   </receiver>
     *   
	 * @param context
	 * @param skus
	 */
	public AmazonBilling(Context context, Set<String> skus) {
		this.skus = skus;
		observer = new BillingObserver(context);
		prefs = new Prefs(context);
	}

	@Override
	public void setupBilling() {
		PurchasingManager.registerObserver(observer);
	}

	@Override
	public void purchaseItem(String sku, String type) {
		requestId = PurchasingManager.initiatePurchaseRequest(sku);
	}

	@Override
	public void setBillingListener(OnBillingEvent listener) {
		billingListener = listener;
	}
	
	private class BillingObserver extends BasePurchasingObserver {

		private String currentId;

		public BillingObserver(Context context) {
			super(context);
		}

		public void onSdkAvailable(final boolean isSandboxMode) {
		}

		public void onGetUserIdResponse(final GetUserIdResponse response) {
			if (response.getUserIdRequestStatus() == GetUserIdRequestStatus.SUCCESSFUL) {
				currentId = response.getUserId();
				prefs.StoreString(USERID, currentId);
				PurchasingManager.initiatePurchaseUpdatesRequest(getOffset());
			}
		}

		public void onItemDataResponse(final ItemDataResponse response) {
			switch (response.getItemDataRequestStatus()) {
				case SUCCESSFUL_WITH_UNAVAILABLE_SKUS:
				case SUCCESSFUL:
					// Mkae sure our sku is active					
					Set<String> purchasedSkus = new HashSet<String>();
					final Map<String, Item> items = response.getItemData();
					for (final String key : items.keySet()) {
						Item item = items.get(key);
						purchasedSkus.add(item.getSku());						
					}
					
					break;
				case FAILED:
					// TODO the request failed.
					break;
			}
		}

		public void onPurchaseResponse(final PurchaseResponse response) {
			switch (response.getPurchaseRequestStatus()) {
				case SUCCESSFUL:
					billingListener.onBillingEvent(PURCHASE_SUCCESS, response.getReceipt().getSku());					
					break;
				case ALREADY_ENTITLED:
					// TODO
					break;
				case FAILED:
					// TODO
					break;
				case INVALID_SKU:
					// TODO
					break;				
			}
		}

		public void onPurchaseUpdatesResponse(final PurchaseUpdatesResponse response) {
			switch (response.getPurchaseUpdatesRequestStatus()) {
				case SUCCESSFUL:
					for (final String sku : response.getRevokedSkus()) {
						// TODO Remove any features from revoked skus
					}
					ArrayList<String> pSkus = new ArrayList<String>();
					for (final Receipt receipt : response.getReceipts()) {
						switch (receipt.getItemType()) {
							case ENTITLED:
								String sku = receipt.getSku();
								pSkus.add(sku);																
								break;
							default:
								break;
						}
					}
					billingListener.onBillingEvent(INVENTORY_RETURN, pSkus);
					Offset newset = response.getOffset();
					if (response.isMore()) {
						PurchasingManager.initiatePurchaseUpdatesRequest(newset);
					} else {
						prefs.StoreString(OFFSET, newset.toString());
					}
					break;

				default:
					break;
			}
		}

		public String getUserId() {
			return currentId;
		}

		public Offset getOffset() {
			String offSet = prefs.GetString(OFFSET, "");
			if (offSet.equals("")) {
				return Offset.BEGINNING;
			} else {
				return Offset.fromString(offSet);
			}
		}
	}

	@Override
	public void onResume() {
		PurchasingManager.initiateItemDataRequest(skus);
		PurchasingManager.initiateGetUserIdRequest();
	}

	@Override
	public void close() {		
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {		
	}

}
